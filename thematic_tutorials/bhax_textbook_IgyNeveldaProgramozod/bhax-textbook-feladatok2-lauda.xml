<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Lauda!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>Port scan</title>
        <para>
            Mutassunk rá ebben a port szkennelő forrásban a kivételkezelés szerepére!
        </para>
        <para>
            <link xlink:href="https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/ch01.html#id527287">https://www.tankonyvtar.hu/hu/tartalom/tkt/javat-tanitok-javat/ch01.html#id527287</link>
        </para>
        <para>
            Kivételkezelés alatt nyílvánvalóan a vezérlést kiakasztó hibák <emphasis>kezelését</emphasis>
            értjük, azaz például olyan hibákat, mint a <emphasis>"Fájl nem található a megadott útvonalon"</emphasis>
            vagy a <emphasis>"Index out of bounds"</emphasis>. A kivételkezelés során
            eldönthetjük, hogy a programunk futhat-e tovább, akár hibás értékekkel is, vagy sem,
            alapértelmezetten meghal a programunk.
        </para>
        <para>
            Kivételkezelésről már
            sokszor volt vólt a könyvben így ennek most csak egy gyakorlati felhasználása
            fog bemutatásra kerülni.
        </para>
        <para>
            <emphasis role='bold'>Miért lehet fontos feltérképezni egy hoszt elérhető portjait?</emphasis>
        </para>
        <para>
            Rendszergazdaként a legfőbb feladatunk a rendszereink integritásának fenntartása, azaz
            külső tényező (program, mint emberi proxy) ne végezzen olyan műveleteket
            a birtokunkban lévő hálózaton ami "megrontá" a standard működést, például
            illetéktelen ne böngésszen a fájlrendszereken belül.
        </para>
        <para>
            A másik irány nyílván a fenti ellenettje, ha a cég minket valamiyen <emphasis>biztonságtechnikai-szakértőként</emphasis>
            alkalmaz akkor a feladataink köré esik a portszkennelés is, hiszen a feladat
            az, hogy tönkretegyük a rendszert, keressünk rajta támadási pontokat. 
            A portszkennelés önmagában nem működik azonban, csupán akkor ha a program, ami
            a megtalált nyitott portot használja, bugos, tehát van olyan <emphasis>feautre</emphasis>-e
            aminek segítségével, a programon keresztül, nem engedélyezett cselekedeteket hajthatunk végre.
        </para>
        <para>
            Egy adott munkagép portjait különböző módokon térképezhetjük fel. Az iparban
            általában nem szoktak írni portszkennelő programokat, hiszen már szabványossá
            vált (de facto) az <emphasis>nmap</emphasis> program használata, vagy a mostában
            feltörekvő <emphasis>ss</emphasis> programé.
        </para>
        <para>
            Ha sajátot szeretnénk írni, elég naív módon, akkor a feladatot úgy oldjuk meg, 
            hogy próbáljuk példányosítani a Socket osztályt különböző portokon, ha
            sikerül akkor nyílván "nyitott", ráadásul felépült egy TCP kapcsolat is.
        </para>
        <para>
            Ennek a kódja a következő Java nyelven megírva:
        </para>
        <programlisting language='Java'>
            <![CDATA[
import java.net.Socket;
import java.io.IOException;

public class PortScanner
{
    public static void main(String[] args)
    {
        if(args.length < 1)
        {
            System.err.println("Usage: java PortScanner <ip>");
            System.exit(-1);
        }
        
        Socket scannerSocket = null;
        int currentPort = 0;
        while(currentPort <= 1024)
        {
            try
            {
                scannerSocket = new Socket(args[0], currentPort);
                System.out.printf("!! A %d nyitva!!\n", currentPort);
            }
            catch(IOException e)
            {
                System.err.printf("!! A %d nincs nyitva!!\n", currentPort);
            }
            currentPort++;
                
        }
              
    }
}            
]]>
        </programlisting>
        <para>
            Ahogy látható a kivételkezelést használtuk fel a programunk logikájának megvalósítására,
            ha nem sikerül példányosítani az aktuális portra akkor nem nyitottnak tekintjük.
        </para>
    </section>        
    <section>
        <title>AOP</title>
        <para>
           Szőj bele egy átszövő vonatkozást az első védési programod Java átiratába! (Sztenderd védési
feladat volt korábban.)
        </para>
        <para>
            Az első védési program Java átirata megtalálható az Arroway fejezet Homokozó című
            feladatában kidolgozva és bemutatva.
        </para>
        <formalpara>
            <title>Mi az AOP(Aspektus oriéntált programozás)?</title>
            <para>
                
            </para>
        </formalpara>
        <para>
            AZ AOP féle technika lehetőséget ad arra, hogy különböző, például debugoláshoz használt,
            az alkalmazáslogikára hatástalan kódokat kiszervezzünk külön fájlokba a projecten belül,
            tehát például nem kell egy <filename>Logger</filename> osztály tagmetódusát meghívni
            egy fontos osztályt definiáló fájlban hanem azt külön megírjuk (pl AspectJ nyelven),
            így nem bonyolódik feleslegesen tovább a kód, az olvashatóság megmarad.
        </para>
        <para>
            Az AOP mára már úgy tűnhet, hogy teljesen kiment a divatból, és ez a hipotézis
            igaznak is bizonyul néhány szemléletmódban.
        </para>
        <para>
            Manapság az AOP (Javaban és C#-ban például) az úgynevezett <emphasis>Reflection API</emphasis>
            révén él tovább, tehát kifinomodott az AOP, nem úgy használjuk ahogyan
            az elképzelője <emphasis>megálmodta</emphasis>.
        </para>
        <para>
            Ettől függetlenül az iparban történő felhasználása a semmihez konvergál, nagyon, de nagyon
            kivételes esetekben nyúlnak hozzá(régén az AspectJ nyelvet használták hozzá,
            tehát legacy kódos esetek ezek a kivételek pl), bár azoknál is inkább mellőzik. Egyszerűen
            nincs komoly szükség rá és sokkal lassabban halad a feljesztés is ha még pluszba
            kiszervezik az ilyen minimális kódbloatot a programozók. Lásd <link xlink:href="https://www.tiobe.com/tiobe-index/">TIOBE-index</link>,
            a listában nem találhatjuk, bár kezeli az oldal. (AspectJ)
        </para>
        <para>
            Az <emphasis role='bold'><emphasis>első védési feladat Java átirata</emphasis></emphasis> alatt
            a <emphasis>Homokozó</emphasis> feladatban Java-ra átportolt LZW Bináris fát értjük.
        </para>
        <para>
            Ebbe két átszövést is meg fogunk valósítani, előszor kiírjuk mit szúrtunk be a fába,
            majd annak eredményét(ami mindig sikerrel jár), másodszor bejárjuk a fát az <emphasis>AspectJ</emphasis>
            segítségével <emphasis role='bold'>Inorder,Postorder</emphasis> módon is az alapértelmezett
            <emphasis role='bold'>Preorder</emphasis> bejárás mellett.
        </para>
        <important>
            <title>
                <emphasis>AspectJ</emphasis>
            </title>
            <para>
                Az AspectJ egy ősrégi project amit már senki nem akar karbantartani, így nagy
                valószínűséggel nem is lehet majd a közeljövőben használni, jelenleg az Eclipse
                alapítvány "fejleszti". A használatához a következők szükségesek:
            </para>
        </important>
        <itemizedlist>
            <listitem>
                <para>
                    <emphasis role='bold'>Java 1.8 (openjdk-1.8)</emphasis> - 
                    Ha esetleg nem létezne: nézzük meg az Oracle archívumát 
                    majd onnan töltsük le(reg köteles). Igen, az AspectJ 
                    csak ezzel a kiadással fog működni, lásd doksik. (maven megoldás halott(?))
                    Érdemes egy külön virtuális gépet felrakni érte, 
                    hogy ne maszatolja szét jelenlegi telepítésünket.
                </para>
            </listitem>
            <listitem>
                <para>
                    <emphasis role='bold'>Eclipse 4.10</emphasis> - Telepítenünk kell az AJDT-t, ehhez meg
                    csak bizonyos Eclipse verziók felelnek meg.
                </para>
            </listitem>
            <listitem>
                <para>
                    <link xlink:href="https://www.eclipse.org/ajdt/downloads/"><emphasis role='bold'>AJDT</emphasis></link>
                </para>
            </listitem>
        </itemizedlist>
        <para>
            Ha a telepítéssel sikeresen megvagyunk akkor megnézhetjük az elmondottak megvalósítását,
            a LZW fát nem tárgyaljuk, mert ki lett vesézve rengeteg feladaton keresztül, lásd <emphasis role='bold'>Homokozó</emphasis>
            feladat, viszont az AspectJ-ről írunk:
        </para>
        <programlisting language='Java'>
            <![CDATA[

public aspect aop
{
    int depth = 0;
    public pointcut callFun(char b) : call(public void LZWBinaryTree.insert(char)) && args(b);
    before(char b) : callFun(b)
    {
        System.out.println("Adding " + b + " into the Treee");
    }
    after(char b) : callFun(b)
    {
        System.out.println("We have successfully included " + b + " into the Treee");
    }

}            
]]>
        </programlisting>
        <para>
            Az AspectJ segítségével valósítjuk meg itt az AOP-t, azaz logolni szeretnénk minden
            a fába törénő beszúrást, ehhez nem szeretnénk (közvetlenül) belenyúlni az <filename>LZWBinaryTree.java</filename>
            forrásba, tehát írunk AspectJ kódot amit majd annak fordítója beleszúr Java
            állományunkba.
        </para>
        <para>
            Hova tudunk beszúrni? Minden lehetséges <emphasis role='bold'>Join Point</emphasis>-nál,
            tehát például amikor hozzáférünk egy változó tartalmához, vagy függvényhívásnál közvetlen
            a hívás előtt/után, stb. 
        </para>
        <para>
            Azt, hogy itt mi fog lefutni azt <emphasis role='bold'>Advice</emphasis>-nak nevezzük, tehát
            azon utasítások halamzát amelyek az említett blokkban vannak.
        </para>
        <para>
            Ahhoz, hogy megtaláljuk a beszúrások helyét(Join Point) írunk kell egy
            <emphasis role='bold'>Pointcut</emphasis> regex-et ami illeszkedni fog minden olyan
            join pontra amire megadtuk szabályát. A Pointcut minden ilyen találatnál végrehajtásra fog kerülni.
        </para>
        <para>
            Mi itt az <function>insert</function> tagfüggvény hívás előtt és
            meghívása után szeretnénk logolni. Ezért írunk rá egy regex-et <emphasis>pointcut</emphasis> 
            segítségével, mivel hivás előtt és után szeretnénk "tanácsolni" használni:
            a <code><![CDATA[before() : after():]]></code>
            blokkkokat használjuk fel.
        </para>
        <para>
            A beutazáokat erre a mintára építjük fel, csupán két új függvényt kell definiálnunk.
        </para>
        <programlisting language='Java'>
            <![CDATA[
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public aspect aop
{
    int depth = 0;
    
    public pointcut travel(LZWBinaryTree.Node n, PrintWriter os) 
	: call(public void LZWBinaryTree.printTree(LZWBinaryTree.Node, PrintWriter)) && args(n,os);
	
    after(LZWBinaryTree.Node n, PrintWriter os) throws FileNotFoundException : travel(n, os)
    {
    	inOrder(n,new PrintWriter("in-order.txt"));
    	depth = 0;
    	postOrder(n,new PrintWriter("post-order.txt"));
    }
    
    
    public void inOrder(LZWBinaryTree.Node n, PrintWriter p)
    {
    	if (n != null)
        {
            ++depth;
            for (int i = 0; i < depth; ++i)
                p.print("---");
            p.print(n.getValue () + "(" + depth + ")\n");
            inOrder (n.getLeftChild (), p);
            inOrder (n.getRightChild (), p);
            --depth;
        }
    }
    public void postOrder(LZWBinaryTree.Node n, PrintWriter p)
    {
    	if (n != null)
        {
            ++depth;
            postOrder (n.getLeftChild (), p);
            postOrder (n.getRightChild (), p);
            for (int i = 0; i < depth; ++i)
                p.print("---");
            p.print(n.getValue () + "(" + depth + ")\n");
            --depth;
        }
    }
    
    
}            
]]>
        </programlisting>
    </section>

    <section>
        <title>Junit teszt</title>
        <para>
            A <link xlink:href="https://progpater.blog.hu/2011/03/05/labormeres_otthon_avagy_hogyan_dolgozok_fel_egy_pedat">https://progpater.blog.hu/2011/03/05/labormeres_otthon_avagy_hogyan_dolgozok_fel_egy_pedat</link>
poszt kézzel számított mélységét és szórását dolgozd be egy Junit tesztbe (sztenderd védési feladat
volt korábban).
        </para>
        <para>
            A Junit egy keretrendszer ami különböző unit-testek készítéséhez, elemzéséhez, végrehajtásához,..
            alkottak meg Java projectek részére, többnyire használata egy build system-hez
            kötődik(érts avval eggyütt használják), mi itt a <application>maven</application>
            build sys-t fogjuk használni. Jelenlegi verzió: JUnit5.
        </para>
        <para>
            Maga a Junit rendszer modulokból tevődik össze.
        </para>
        <para>
            A Junit Platform project felelőlös a JVM-mel, build systemekkel történő integrációért.
        </para>
        <para>
            A Junit Jupiter project segítségével írhatunk különböző modelleket követő unit teszteket,
            ezt fogjuk használni ebben a feladatban.
        </para>
        <para>
            Régi JUnit platformokért felel a JUnit vintage.
        </para>
        <para>
            <emphasis role='bold'>Mi a feladtunk?</emphasis>
        </para>
        <para>
            A link által megosztott papíroson kiszámolt eredményeket kell leellenőrizünk
            a Arroway fejezet Homokozó feladatában lévő LZWBinárisFa Java megvalósításán egy
            JUnit teszttel
        </para>
        <para>
            A JUnit lehetőséget ad arra, hogy annotációk segítségével a tesztjeinket a program
            élete bármely pillanatában eltudjuk végezni. A forrásban csupán pár ilyen
            annotációt használnunk fel, továbbiakért érdemes meglátogatni a doksikat.
        </para>
        <para>
            Mielőtt lefutatjuk a teszteket győzödjönk meg róla, hogy a teszt állományunk
            neve *Test.java-ra végződik, ellenben nem fogja lefuttatni a maven rendszer.
        </para>
        <para>
            A tesztek futtatásához a pom fájlban függőségként meg kell adnunk a Junit Jupiter
            projectet, lásd mvn repo, és egy plugint is ami lefutattja a teszteket, mi itt
            a <emphasis>Surefire</emphasis> plugint vesszük igénybe.
        </para>
        <programlisting language='Java'>
            <![CDATA[
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.targear</groupId>
    <artifactId>JUnit</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>jar</packaging>
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>13</maven.compiler.source>
        <maven.compiler.target>13</maven.compiler.target>
    </properties>
    <build>
    <plugins>
        <plugin>
            <artifactId>maven-surefire-plugin</artifactId>
            <version>2.22.2</version>
        </plugin>
    </plugins>
</build>
    <dependencies>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <version>5.5.2</version>
            <artifactId>junit-jupiter-engine</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>
</project>            
]]>
        </programlisting>
        <para>
            Most lássuk a tesztünket!
        </para>
        <para>
            Mivel a JUnit 5-s verzióját használom már helyeken találhatóak meg
            például az <function>assertEquals</function> függvény, először beimportáljuk
            őket:
        </para>
        <programlisting language="Java">
            <![CDATA[
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author cantor
 */
public class LZWUnitTest {
    
    LZWBinaryTree testTree = new LZWBinaryTree();
    String input = "01111001001001000111";
    ]]>
    </programlisting> 
    <para>
        A tesztünkben példánysítani kell nyílvánvalóan a tesztelni óhajtott
        osztályt, ezt meg is tesszük, illetve eltároljuk a linken található számsorozatot
        egy String példányban, ezt fogjuk olvasgatni.
    </para>
    <programlisting language="Java">
            <![CDATA[
    @BeforeAll
    public static void startTesting() 
    {
        System.out.println("I'm about to test this tree.");
       
    }
    
    @AfterAll
    public static void endTesting() 
    {
        System.out.println("It's all good.");
    }
    ]]>
    </programlisting> 
    <para>
        Itt láthatjuk először a Junit annotáció használatát. Ez a két függvény csupán
        "szemantikai" cukor szerepet töltenek be, jelzem vele, hogy megkezdődik e
        unit-test tesztjeinek végrehajtása és végét.
    </para>
    <para>
        Ezeknél az annotációknál fontos, hogy a metódusok statikusak legyenek, ha nem
        azok a JUnit logikáját sértenénk meg.
    </para>
    <para>
        Megjegyzés: Az @Beforeall/@AfterAll minden esetben le fog futni, ammenyiben futtatuk a tesztet.
    </para>
    <programlisting language="Java">
            <![CDATA[
    @Test
    public void testAverage()
    {
        for(char b : input.toCharArray())
        {
            testTree.insert(b);
        }
        
        assertEquals(2.75, testTree.getMean(), 0.0001);
        System.out.println("A várt átlag megegyezik a kapottal!");
        System.out.printf("Várt: %f Kapott: %f\n", 2.75,testTree.getMean());
    }
    @Test
    public void testDepth()
    {
        for(char b : input.toCharArray())
        {
            testTree.insert(b);
        }
        assertEquals(4 , testTree.getDepth());
        System.out.println("A várt mélység megegyezik a kapottal!");
        System.out.printf("Várt: %d Kapott: %d\n", 4,testTree.getDepth());
    }
    @Test
    public void testVariance()
    {
        for(char b : input.toCharArray())
        {
            testTree.insert(b);
        }
        assertEquals(0.9574, testTree.getVariance(), 0.0001);
        System.out.println("A várt szórás megegyezik a kapottal!");
        System.out.printf("Várt: %f Kapott: %f\n", 0.9574,testTree.getVariance());
    }
}
            
]]>
        </programlisting>
        <para>
            Ezek a tényleges tesztek, ezeket kötelezően meg kell jelölni a @Test
            annotációval különben nem kerülnek futtatásra.
        </para>
        <para>
            Fontos megjegyezni, hogy minden egyes teszt esetén a tesztelendő
            objektum különböző, azaz tesztenként újra legyártja itt az <filename>LZWBinaryTree</filename>
            példányt. Ez amiatt szükséges, hogy a tesztek függetlenek legyenek egymástól, főleg
            ha csak egy konkrét függvényt akarunk tesztelni(pl: lehet egy másik függvény "kiveszi"
            az inputból azt a karaktert ami nem várt viselkedés eredménezne, így helytelen lenne
            a teszt)
        </para>
        <para>
            A fenti ok miatt látjuk azt, hogy minden egyes tesztnél "feltöltjük" a fát
            az inputtal mielőtt elvégeznénk magát a tesztet.
        </para>
        <para>
            Észrevehető az is, hogy egyfajta delta értéket adtunk meg két teszt esetében is.
            Ez a lebegőpontos számok miatt szükséges, hiszen azok implementációja nem
            azonos platformok között illetve az aritmetikájuk sem, így a törtrészértékek
            nem biztos, hogy abszolút azonosak lesznek, valamennyiben el fognak térni.
            Ez a delta érték a megengedett eltérés mértéke, ha eltérnek de maximum csak ennyire
            akkor egyenlőknek tekintjük őket.
        </para>
        <para>
            Ezekután futassuk le a teszteket például a <command>mvn package</command>
            parancs meghívásával ami a <emphasis>test</emphasis> fázist is végrehajtja.
        </para>
    </section>       
    
    <section>
        <title>OSCI</title>
        <para>
            Készíts egyszerű C++/OpenGL-es megjelenítőt, amiben egy kocsit irányítasz az úton.
        </para>
        <para>
            A feladat az, hogy készítsünk egy "játékot" amiben egy kocsit irányítunk egy úton.
            Tanár úr javaslatára én Unity játékmotort használtam a feladat megoldására, hiszen
            ez nagyon jól szemlélteti az "óriások vállán állunk" filozófiát, azaz ne
            kezdjünk megírni "gyakori" problémákra a megoldást, hanem használjunk kipróbált,
            bizonyítottan helyes implementált és biztosan a lehető legjobb teljesítménnyel bíró
            ipari megoldásokat.
        </para>
        <para>
            Kezdetnek be kell szerezni a Unity fejlesztőplatformot, ezt elérhetjük
            a <link xlink:href="https://unity.com/">weboldalukon</link> keresztül vagy ha támogatja rendszerünk
            csomagkezelői közül valaki akkor a csomagkezelőn keresztül.
        </para>
        <para>
            Miután feltelepítettük ajánlatos feltenni, és a megoldás is erre épül, 
            a <emphasis role="bold">Standard Assets</emphasis> kiegészítőt az <emphasis>AssetsStore</emphasis>-ból,
            illetve, amennyiben még létezik, az <emphasis>EasyRoads3D</emphasis> assetet.
        </para>
        <para>
            Először is leraktam 6 darab terrain objektumot, ezek alapértelmezetten
            textúra nélkül jelennek meg, értsd szemet megvakító fehérséggel találjuk szembe magunkat
            lerakásukkor.
        </para>
        <para>
            A terrain objektumok rendelkeznek egy "extrusion" szerű eszközzel amivel
            kiemelhetjük a sík egyes részeit, illetve besüllyeszthetjük. Többféle
            ecset közül választhatunk. Ezzel az eszközzel alakítottam ki a képen látható
            pálya széleit, egyfajta játékvilág határként szolgáló dombokat/hegyeket.
        </para>
        <para>
            Nem kell aggódnunk a collision detection miatt, ugyanis alapértelmezetten
            "nem jut át" objektum a terrainen.
        </para>
        <para>
            Ezután alkalmaztam egy <emphasis>Grass</emphasis> típusú textúrát a síkra.
        </para>
        <formalpara>
            <title>Úthálózat kialakítása</title>
            <para>
                
            </para>
        </formalpara>
        <para>
            Az <emphasis>EasyRoad</emphasis> kiegészítőt felhasználva egyszerű volt az úthálózat kiépítése.
            Az alapértelemzetten 5 egység széles utat lecseréltem egy 30(külső) és 20(belső, rejtett hely)
            szélességű utakra.
        </para>
        <para>
            A hálózat kiépítése nagyon egyszerű volt, a kiegészítő használatához csupán a Shift billenytű
            lenyomása mellett kellett kattingatnunk a terrainen ezzel markereket letéve. Az asset erre
            a markerekre automatikusan görbét illesztett, majd erre rákerült az út.
        </para>
        <para>
            Utolsó lépésként még ráraktam a kocsit, töröltem a MainCamera-t(hogy az autóé legyen
            az alapértelmezett). A Unity kb 5 perc alatt lefordíotta a projectet és már
            "játszható" is volt.
        </para>
        <para>
            A Unity alapértelmezetten
            C# szkriptekkel dolgozik amik a játékvilágot és objektumait tudják manipulálni.
            Egy ilyen szkript két fő eleme a <function>setup</function> és <function>update</function>
            függvények, előbbi egyszer fut le, inicializáláskor, a másik bizonyos időközönként,
            ahogy a neve is elárulja.
        </para>
        <para>
            Itt egy kódrészlet a kocsi mozgatására szemléletés képpen:
        </para>
        <programlisting language="C">
            <![CDATA[
 public void Move(float steering, float accel, float footbrake, float handbrake)
        {
            for (int i = 0; i < 4; i++)
            {
                Quaternion quat;
                Vector3 position;
                m_WheelColliders[i].GetWorldPose(out position, out quat);
                m_WheelMeshes[i].transform.position = position;
                m_WheelMeshes[i].transform.rotation = quat;
            }
]]>
            </programlisting>
            <para>
                Mi történik itt? Az autót, ahogy valahogyan a valóságban is, kerekeinek
                gördülése viszi előre, itt mind a négy kereket(textúrát) transzformáljuk
                a keréknek jelenlegi fizikai helyzetének megfelelően.
            </para>
            <para>
                Láthatni, hogy az <command>out</command> kulcsszó jelöli azokat a változókat
                amikbe a <function>GetWorldPos</function> függvény fog értékeket rakni.
            </para>
            <para>
                A továbbiakban a fizika egy implementációját olvashatjuk,
                ez adja meg, hogy viselelkedjen az autó gyorsulás közben(pl gyorsulás mértéke),
                vagy hogy hogyan is nézzen ki az ellenállás ami akadályozza haladását, stb..
            </para>
            
            <programlisting language="C">
<![CDATA[
            //clamp input values
            steering = Mathf.Clamp(steering, -1, 1);
            AccelInput = accel = Mathf.Clamp(accel, 0, 1);
            BrakeInput = footbrake = -1*Mathf.Clamp(footbrake, -1, 0);
            handbrake = Mathf.Clamp(handbrake, 0, 1);

            //Set the steer on the front wheels.
            //Assuming that wheels 0 and 1 are the front wheels.
            m_SteerAngle = steering*m_MaximumSteerAngle;
            m_WheelColliders[0].steerAngle = m_SteerAngle;
            m_WheelColliders[1].steerAngle = m_SteerAngle;

            SteerHelper();
            ApplyDrive(accel, footbrake);
            CapSpeed();

            //Set the handbrake.
            //Assuming that wheels 2 and 3 are the rear wheels.
            if (handbrake > 0f)
            {
                var hbTorque = handbrake*m_MaxHandbrakeTorque;
                m_WheelColliders[2].brakeTorque = hbTorque;
                m_WheelColliders[3].brakeTorque = hbTorque;
            }


            CalculateRevs();
            GearChanging();

            AddDownForce();
            CheckForWheelSpin();
            TractionControl();
        }


        private void CapSpeed()
        {
            float speed = m_Rigidbody.velocity.magnitude;
            switch (m_SpeedType)
            {
                case SpeedType.MPH:

                    speed *= 2.23693629f;
                    if (speed > m_Topspeed)
                        m_Rigidbody.velocity = (m_Topspeed/2.23693629f) * m_Rigidbody.velocity.normalized;
                    break;

                case SpeedType.KPH:
                    speed *= 3.6f;
                    if (speed > m_Topspeed)
                        m_Rigidbody.velocity = (m_Topspeed/3.6f) * m_Rigidbody.velocity.normalized;
                    break;
            }
        }            
]]>
        </programlisting>
    </section>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>           