<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Liskov!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>Liskov helyettesítés sértése</title>
        	<para>
				Írjunk olyan OO, leforduló Java és C++ kódcsipetet, amely megsérti a Liskov elvet! Mutassunk rá a
				megoldásra: jobb OO tervezés.
			</para>
        <para>
            Ha egy programban egy adott <literal>T</literal> altípusa <literal>S</literal> akkor
            minden olyan helyen ahol <literal>T</literal> használható, lecserélhető <literal>T</literal>
            <literal>S</literal> altípusával anélkül, hogy az hatással lenne a 
            <emphasis role='bold'>program tulajdonságára.</emphasis>
        </para>
        <para>
            Azt már láttuk példákon keresztül, hogy bármilyen ős lecsélhető valamely gyermekébe,
            viszont ez a tény önmagában nem elégíti a Liskov-elvet, hiszen nincs garancia
            arra, hogy a program eredeti viselkedése/tulajdonsága megmarad.
        </para>
        <para>
            Legegyszerűbb példa az, ha azt mondjuk, hogy adott egy <literal>Rectangle</literal>
            osztály és annak egy gyermeke a <literal>Square</literal>. Egy téglalap oldalainak hossza
            tetszőlegesen hosszú lehet, nem kötelező egyforma hosszúnak lenniük.
            A négyzet definíció szerint téglalap, viszont vannak megkötései,
            oldalainak hosszának meg kell egyeznie. Mi a <literal>Rectangle</literal>
            osztályunkban implementáltunk két olyan függvényt amik külön-külön
            módosítják a téglalap objektum szélességét/magasságát, a <literal>Square</literal>
            örökli ezeket a metódusokat, és láthatjuk már ebből előre, hogy megsértjük
            a Liskov-elvet, nem lesz használható azokon a helyeken a <literal>Square</literal>
            ahol a <literal>Rectangle</literal> használható volt.
        </para>
        <para>
            Tegyük fel, hogy van olyan függvény/metódus valahol a programunkban, aminek
            csak a téglalapunk szélességét van joga megváltoztatni. Ha a téglalap helyett
            argumentumként egy négyzetet adnánk meg, akkor annak magassága is változna, tehát
            a programunk helytelenül működne, megsértettük az Elvet.
        </para>
        <para>
            Akkor is megsértenénk az Elvet, ha 
            megvizsgálnánk, hogy milyen típusú a megadott argumentum, hiszen explicit
            megnéznénk, hogy elfogadható típusú objektumot kaptunk-e, miközben a feltétel
            az, hogy ahol megengedett egy szupertípus használata, ott az összes gyermekének
            is használhatónak kell lennie.
        </para>
        <para>
            Konkrét példának vizsgáljuk meg a <emphasis>Madarak</emphasis> objektumorientáltságát!
        </para>
        <programlisting language='Java'>
            <![CDATA[
class Bird
{
    public void  fly()
    {
		System.out.println("Am flying...\n");
    }
}

class Eagle extends  Bird
{
    public void fly()
    {
        System.out.println("Eagle: flying..\n");
    }
}
class Penguin extends  Bird
{
}

class Liskov
{

    public static void flyBird(Bird b)
    {
        b.fly();
    }

    public static void main(String[] args)
    {
        Bird theEagle = new Eagle();
        Bird thePenguin = new Penguin();
        
        flyBird(theEagle);
        flyBird(thePenguin);
        
        
    }

}            
]]>
        </programlisting>
        <para>
            A hiba itt az, hogy a pingvinek nem tudnak repülni. Azonban a program alapján mégis repülnek, hiszen sikeresen
            meghívódik rajtuk a <function>fly</function> metódus, mert örökölték
            az Őstől, Liskov-elv itt megsérül, a programunknak csak a repülő madarakat
            kellene, hogy megreptesse. A hiba oka az, hogy mi minden
            madárról azt feltételeztük, hogy képes repülni, így
            a <function>fly</function> funkcionalitást mindegyik örökli.
        </para>
        <para>
            Megoldásként kivesszük az Őstől a <function>fly</function> metódust és bevezetünk
            két új interfészként szolgáló osztályt. Az egyik <literal>IFlyingBird</literal>
            lesz, ami deklarálja a virtuális <function>fly</function> metódust,
            másik a <literal>INotFlyingBird</literal> lesz, végül lecseréljük
            a <function>flyBird</function> statikus paraméterének típusát
            <literal>IFlyingBird</literal>re.
        </para>
        <para>
            A fenti átalakítások után már a Liskov-elvet nem fogja érni bántalmazás,
            hiszen csakis repülni képés fajok lehetnek argumentumai.
        </para>
        <programlisting language='Java'>
            <![CDATA[
interface Bird
{
    
}

interface IFlyingBird extends Bird
{
    public void  fly();
}
interface INotFlyingBird extends Bird
{
}
class Eagle implements IFlyingBird
{
    public void fly()
    {
        System.out.println("Eagle: flying..\n");
    }
}
class Penguin implements INotFlyingBird
{
}

class Liskov
{

    public static void flyBird(IFlyingBird b)
    {
        b.fly();
    }

    public static void main(String[] args)
    {
        Eagle theEagle = new Eagle();
        Penguin thePenguin = new Penguin();
        
        flyBird(theEagle);
        flyBird(thePenguin); //Fordítási hiba!!
        
        
    }

}            
]]>
        </programlisting>
        <para>
            Végül a C++ átiratok:
        </para>
		<para>
            Liskov-elv sértve:
        </para>
        <programlisting language='c++'>
            <![CDATA[
#include <iostream>

class Bird
{
    
public:
   
    virtual void  fly()
    {
        std::cout << "Am flying...\n" ;
    }
};

class Eagle : public Bird
{
public:
  
    void fly() override
    {
        std::cout << "Eagle: flying..\n";
    }
};

class Penguin : public Bird
{
};

static void flyBird(Bird& b)
{
    b.fly();
}

int main()
{
    Eagle theEagle;
    Penguin thePenguin;
    
    flyBird(theEagle);
    flyBird(thePenguin);
    
    return 0;
}
            
]]>
        </programlisting>
        <para>
            Liskov-elv tisztelve:
        </para>
         <programlisting language='c++'>
            <![CDATA[
#include <iostream>

class Bird
{
    
};

class IFlyingBird : public Bird
{
public:
    virtual void fly() = 0; //  P U R E  V I R T U A L  F U N C T I O N
};
class INotFlyingBird : public Bird
{
};

class Eagle : public IFlyingBird
{
public:
  
    void fly() override
    {
        std::cout << "Eagle: flying..\n";
    }
};

class Penguin : public INotFlyingBird
{
};

static void flyBird(IFlyingBird& b)
{
    b.fly();
}

int main()
{
    Eagle theEagle;
    Penguin thePenguin;
    
    flyBird(theEagle);
    //flyBird(thePenguin); //Ez az utasításmár nem fog lefordulni...
    //A programtulajdonságai épek maradnak.
    
    return 0;
}
            
]]>
        </programlisting>

    </section>

    <section>
        <title>Szülő-gyerek</title>
        	<para>
				Írjunk Szülő-gyerek Java és C++ osztálydefiníciót, amelyben demonstrálni tudjuk, hogy az ősön
				keresztül csak az ős üzenetei küldhetőek!
			</para>
			<para>
				Ez a feladat csupán azt demonstrálná, hogy nem lehetséges egy adott szülő referencián
				keresztül, ami egy gyerek objektumára hivatkozik, meghívni gyermeke egy olyan metódusát
				amit ő maga nem definiált.
			</para>
			<para>
				C++-ban, Javaban is, ezt polimorfizmussal tudjuk kimutatni, eleve polimorfizmusról
				beszélünk ha egy szülő mutató vagy referencia egy gyerekére mutat/hivatkozik.
				Polimorfizmus alatt ebben a kontextusban azt értjük, hogy több különböző
                típusú objektumhoz hozzá tudunk férni egy közös interfészen (Ősön) keresztül, és az
                Ős típusú változón keresztül meghívhatjuk az Őssel közös metódusokat rajtuk. 
            </para>
			<para>
				A nem Ősök által definiált metódusokhoz nem férhetünk hozzá,
				hacsak nem <emphasis>downcastoljuk</emphasis> az adott objektumot
				a tényleges típusára. Ez esetben viszont megsértjük az előző feladatban ismertetett
				Liskov-elvet.
			</para>
        <programlisting language='C++'>
            <![CDATA[
#include <iostream>
#include <string>

class Szulo
{
public:
        void printValamit()
    {
        std::cout << "Szülő mond valamit\n";
    }
};
class Gyerek : public Szulo
{
public:
        void echoValamit(std::string msg)
    {
       std::cout << msg << "\n";
    }
};



class App
{
   int main()
    {
        Szulo* p = new Szulo();
        Szulo* p2 = new Gyerek();
        
        std::cout << "Szülő metódus meghívása\n";
        p->printValamit();
        
        std::cout << "Gyerek metódus meghívása szülőn keresztül\n";
        p2->echoValamit("Nem jó");
        
        delete p;
        delete p2;
        
    }
};

]]>
        </programlisting>
		<para>
			A <varname>p2</varname> szemantikája alapján csakis a <literal>Szulo</literal> őstől
			örökölt metódusokat képes meghívni <literal>Gyerek</literal> gyermekén.
		</para>
        <para>
            Javaban ugyanígy:
        </para>
        <programlisting language='Java'>
            <![CDATA[
class Szulo
{
    public void printValamit()
    {
        System.out.println("Szülő mond valamit);
    }
}
class Gyerek extends Szulo
{
    public void echoValamit(String msg)
    {
        System.out.println(msg);
    }
}
public class App
{
    public static void main(String[] args)
    {
        Szulo p = new Szulo();
        Szulo p2 = new Gyerek();
        
        System.out.println("Szülő metódus meghívása");
        p.printValamit();
        
        System.out.println("Gyerek metódus meghívása szülőn keresztül");
        p2.echoValamit("Nem jó");
    }
}
            
]]>
        </programlisting>
        <para>
			A <varname>p2</varname> szemantikája alapján csakis a <literal>Szulo</literal> őstől
			örökölt metódusokat képes meghívni <literal>Gyerek</literal> gyermekén.
		</para>

    </section> 

    <section>
        <title>Anti OO</title>
        	<para>
				A BBP algoritmussal a Pi hexadecimális kifejtésének a 0. pozíciótól számított
				10<superscript>6</superscript>, 10<superscript>7</superscript>,10<superscript>8</superscript>
				jegyét határozzuk meg C, C++, Java és C# nyelveken és vessük össze a futási időket!
			</para>
			<para>
				A Bailey-Borwein-Plouffe algoritmus segítségével kiszámolhatjuk a &#x03C0; értékét tetszőleges
				hexadecimális/bináris számjegyétől kezdődően, a BPP formula felhasználásával.
				Az algoritmus, a log<subscript>2</subscript> bináris számjegyeinek kiszámításán alapul.
			</para>
        <programlisting language='C'>
            <![CDATA[
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

char hexLetters[] = {'A','B','C','D','E', 'F'};

long nMod(int n, int k)
{
		int t = 1;
		
		while(t <= n)
		{
			t = 2*t;
			
		}
		long r = 1;
		
		while(1)
		{
			if(n >= t)
			{
				r = 16*r % k;
				n = n - t;
			}
			t/=2;
			if(t < 1)
				break;
			if(t>=1)
			{
				r = r*r % k;
			}
		}
		
		return r;
		
}

double getS(int d, int j)
{
		double ret = 0.0;
		
		for(int i=0; i<d; i++)
		{
			ret+= (nMod(d-i, 8*i+j))/(double)(8*i+j);
			
		}
		
		return ret - floor(ret);
}
void calculate(int n)
{   
    double pi = 0.0;
    double pi1 = 4* getS(n,1);
    double pi2 = 2*getS(n,4);
    double pi3 = getS(n,5);
    double pi4 = getS(n,6);
    
    pi = pi1-pi2-pi3-pi4;
    pi = pi-floor(pi);
    int hex = floor(16.0*pi);
    printf("%c\n",((hex < 10) ? (char)((hex) + '0') : hexLetters[hex-10]));   
}
int main(int argc, char* argv[])
{
        if(argc == 1)
        {
            printf("Usage: ./BPP <exponent>\n");
            exit(-1);
        }
        int exponent = atoi(argv[1]);
        if(!exponent)
        {
            printf("Usage: ./BPP <exponent>\n");
            exit(-1);
        }
        printf("10^%d\n", exponent);
        int limit = pow(10,exponent);
        calculate(limit);				
		
}
            
]]>
        </programlisting>
        <programlisting language='C++'>
            <![CDATA[
#include <iostream>
#include <string>
#include <cmath>

char hexLetters[] = {'A','B','C','D','E', 'F'};

long nMod(int n, int k)
{
    int t = 1;
    
    while(t <= n)
    {
        t = 2*t;
        
    }
    long r = 1;
    
    while(true)
    {
        if(n >= t)
        {
            r = 16*r % k;
            n = n - t;
        }
        t/=2;
        if(t < 1)
            break;
        if(t>=1)
        {
            r = r*r % k;
        }
    }
    
    return r;
    
}
double getS(int d, int j)
{
    double ret = 0.0;
    
    for(int i=0; i<d; i++)
    {
        ret+= (nMod(d-i, 8*i+j))/(double)(8*i+j);
        
    }
    
    return ret - std::floor(ret);
}
void calculate(int n)
{
    double pi = 0.0;
    double pi1 = 4* getS(n,1);
    double pi2 = 2*getS(n,4);
    double pi3 = getS(n,5);
    double pi4 = getS(n,6);
    
    pi = pi1-pi2-pi3-pi4;
    pi = pi-std::floor(pi);
    
    int hex = std::floor(16.0*pi);
    std::cout << ((hex < 10) ? (char)(hex + '0') : hexLetters[hex-10]);
}
int main(int argc, char* argv[])
{
    if(argc == 1)
    {
        std::cout <<"Usage: ./BPP <exponent>\n";
        exit(-1);
    }
    int exponent = std::atoi(argv[1]);
    if(!exponent)
    {
        std::cout << "Usage: ./BPP <exponent>\n";
        exit(-1);
    }
    std::cout << "10^" << exponent << "\n";
    long long limit = std::pow(10,exponent);
    
    calculate(limit);
    
    std::cout << std::endl;
	return 0;	
		
}
            
]]>
        </programlisting>
        <programlisting>
            <![CDATA[
using System;

namespace CSHARP
{
    class Program
    {
        public static double getS(int d, int j) 
        {
            
            double ret = 0.0d;
            
            for(int k=0; k<=d; ++k)
                ret += (double)nMod(d-k, 8*k + j) / (double)(8*k + j);
            
            
            return ret - Math.Floor(ret);
        }
    
        public static long nMod(int n, int k) 
        {
            
            int t = 1;
            while(t <= n)
                t *= 2;
            
            long r = 1;
            
            while(true) {
                
                if(n >= t) {
                    r = (16*r) % k;
                    n = n - t;
                }
                
                t = t/2;
                
                if(t < 1)
                    break;
                
                r = (r*r) % k;
                
            }
            
            return r;
        }
        public static void calculate(int n)
        {
            char[] hexLetters = {'A','B','C','D','E','F'};
            double pi = 0.0;
            double pi1 = 4* getS(n,1);
            double pi2 = 2*getS(n,4);
            double pi3 = getS(n,5);
            double pi4 = getS(n,6);
            
            pi = pi1-pi2-pi3-pi4;
            pi = pi-Math.Floor(pi);
            int hex = (int)Math.Floor(16.0*pi);
            Console.WriteLine("A {0}. pozíción lévő jegye {1}",n, ((hex < 10) ? (char)((hex) + '0') : hexLetters[hex-10]));
        }
    
        public static void Main(String[] args)
        { 
            if(args.Length == 0)
            {
                Console.WriteLine("Usage: dotnet run <exponent>");
                Environment.Exit(-1);
            }
            try
            {
                Console.WriteLine("10^" + Convert.ToInt32(args[0]));
                calculate((int)Math.Pow(10,Convert.ToInt32(args[0])));
            }
            catch (FormatException e)
            {
                Console.WriteLine(e);
                Console.WriteLine("Usage: dotnet run <exponent>");
                Environment.Exit(-1);
            }
           
        }
            
        }
    }            
]]>
        </programlisting>
        <programlisting language='Java'>
            <![CDATA[
class BPP
{
	
	static char[] hexLetters = {'A','B','C','D','E', 'F'};
	
	public static long nMod(int n, int k)
	{
		int t = 1;
		
		while(t <= n)
		{
			t = 2*t;
			
		}
		long r = 1;
		
		while(true)
		{
			if(n >= t)
			{
				r = 16*r % k;
				n = n - t;
			}
			t/=2;
			if(t < 1)
				break;
			if(t>=1)
			{
				r = r*r % k;
			}
		}
		
		return r;
		
	}
	
	public static double getS(int d, int j)
	{
		double ret = 0.0;
		
		for(int i=0; i<d; i++)
		{
			ret+= (nMod(d-i, 8*i+j))/(double)(8*i+j);
			
		}
		
		return ret - Math.floor(ret);
	}
	public static void calculate(int n)
	{
        double pi = 0.0;
        double pi1 = 4* getS(n,1);
        double pi2 = 2*getS(n,4);
        double pi3 = getS(n,5);
        double pi4 = getS(n,6);
        pi = pi1-pi2-pi3-pi4;
        pi = pi-Math.floor(pi);
                
        int hex = (int)StrictMath.floor(16.0*pi);
        System.out.print(((hex < 10) ? String.valueOf(hex) : hexLetters[hex-10]));
	}
	public static void main(String[] args)
	{
        if(args.length == 0)
        {
            System.out.println("Usage: java BPP <exponent>");
            System.exit(-1);
        }
        try
        {
            System.out.println("10^" + args[0]);
            int limit = (int)Math.pow(10,Integer.parseInt(args[0]));
            calculate(limit);
           
        }
        catch(Exception e)
        {
            System.out.println("Usage: java BPP <exponent>");
        }
      	
	}
}            
]]>
        </programlisting>


    </section>

    <section>
        <title>Hello, Android!</title>
        	<para>Élesszük fel az SMNIST for Humans projektet!
            <link xlink:href="https://gitlab.com/nbatfai/smnist/tree/master/forHumans/SMNISTforHumansExp3/app/src/main">https://gitlab.com/nbatfai/smnist/tree/master/forHumans/SMNISTforHumansExp3/app/src/main</link>
            Apró módosításokat eszközölj benne, pl. színvilág.
        </para>
        <para>
            Ahhoz, hogy felélesszük az említett projektet legelőször is rendelkeznünk kell valamilyen
            Android IDE-vel(bár ez sem feltétlenül szükséges).
        </para>
        <para>
            Ha sikerült feltelepítenünk az általunk preferált <emphasis role='bold'>GNU/Linux</emphasis>
            rendszerünkre akkor első indításkor egy konfigurációs ablak fogad minket, itt
            a tényleges munkánk megkezdése előtt beállíthatjuk az IDE-t.
        </para>
        <para>
            Az Android alkalmazásainkat tudjuk debugolni akár saját Androidos készülékeinken is,
            vagy akár emulátor segítségével is. Ha emulátort szeretnénk használni már
            itt a konfigurációs űrlapnál kiválaszthatunk egyet feltelepítésre.
        </para>
                <para>
				A legújabb Android platform fejlesztéséhez szükséges csomagok települnek alapértelmezetten,
				de van lehetőség arra, hogy mi régebbi sdk-kat használjunk.
        </para>
		<para>
			A <filename>.grade</filename> fájl nélkül nem tudod importálni a projectet, ennél
			a projectnél kimondottan szinte sehogy sem kényelmesen az importálás,
			mert ennek a repojába csakis a forrásfájlok
			vannak feltöltve és semmi információt nem találsz a build-ről.
		</para>
		<para>
			Ekkor létrehozunk egy <emphasis>empty</emphasis> Android projectet és behozzuk
			a leklónozott forrásokat.
		</para>
        <para>
            A legelső lehetőséggel fogjuk kiásni digitális sírjából a megcsonkított projektet,
			majd beállítjuk a standard dolgokat.
		</para>
		<para>
			Első lépésként átmásoljuk a <filename>activity_smniste3.xml</filename> fájlt
			a <literal>*/layout</literal> mappába.
		</para>
        <para>
            Ahhoz, hogy az aktivitásunk ezt a <emphasis>layout</emphasis>-ot használja át kell írni
            az <filename>AndroidManifest.xml</filename> fájlt:
        </para>
        <programlisting language='xml'>
            <![CDATA[
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.example.forhumans">

    <application
        android:allowBackup="true"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:supportsRtl="true"
        android:theme="@style/AppTheme">
        <activity android:name=".SMNISTE3Activity">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
    </application>

</manifest>            
]]>
        </programlisting>
		<para>
			A többi erőforrás berendelése után elkezdjük javítani a hibákat, a képen látható,
			hogy már az eredeti csomagneveket lecseréltük az aktuális új wrapper project struktúra
			javára.
		</para>
        <para>
            Az Android SDK-k egyik negatív jellemzője, hogy nagyon gyorsan változnak, mármint
            nem csak featurekben, hanem szintaxisban és struktúrában, így hamar, akár 3-4 hónapon belül
            máshova kerülhetnek a már gyakorlatban kipróbált, megtalált osztályaink. 
        </para>
        <para>
            Például az eredeti állományban szereplő 
            <code><![CDATA[public class SMNISTE3Activity extends android.support.v7.app.AppCompatActivity]]></code> sor eseténél:
        </para>
        <programlisting language='Java'>
            <![CDATA[
...            
package com.example.forhumans;
import androidx.appcompat.app.AppCompatActivity;

public class SMNISTE3Activity extends AppCompatActivity {
...        
]]>
        </programlisting>
        <para>
            A színvilágot a <filename>SMNISTSurfaceView.java</filename> állományban a megfelelő változók értékeinek megváltoztatásával változtathatjuk meg.
        </para>
        <para>
            Ugyanitt megváltoztathatjuk a különféle <emphasis>shapek</emphasis> geometriátját is.
        </para>
        <para>
            Legelején említettem, hogy lehet akár közvetlenül hardveren debugolni, ezt úgy tudjuk
            elérni, hogy csatlakoztatjuk a rendszerünkhöz az eszközt <emphasis role='bold'>USB-DEBUGGING</emphasis>
            opció bekapcsolása mellett, majd kiválasztjuk az debug eszközt az IDE-ben.
        </para>
         
    </section>
        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                