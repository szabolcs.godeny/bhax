#include <stdio.h>

void convert(int a) {
	for (int i = 0; i < a; i++) {
		printf("1");
	}
	printf("\n");
}

int main (void) {
	printf("Input:\n");
	int a;
	scanf("%i", &a);
	convert(a);
	return 0;
}