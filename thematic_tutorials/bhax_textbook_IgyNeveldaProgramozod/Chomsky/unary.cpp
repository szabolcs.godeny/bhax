#include <iostream>
#include <limits>

void convertDecimalToUnary(int val);


int main()
{
    int val, theOption;
    
    std::cout << "Please enter a value to convert to unary:\n";
    
    do
    {
        std::cin >> val;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        
    }while(std::cin.fail());
    
    convertDecimalToUnary(val);
    
    return 0;
    
    
}

void convertDecimalToUnary(int val)
{
    for(int i=0; i<val; i++)
    {
        std::cout << "1";
    }
    std::cout << "\n";
}
