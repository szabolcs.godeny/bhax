import java.io.*;
import java.awt.Color;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

public class ascii{

	public static void convert(BufferedImage image){
		for(int i = 0; i < image.getHeight(); ++i){
			for (int j = 0; j < image.getWidth(); j++) {
				Color pixcol = new Color(image.getRGB(j, i));
				double pixval = ((pixcol.getRed() * 0.3) + (pixcol.getGreen() * 0.59) + (pixcol.getBlue() * 0.11));
				System.out.print(karakter(pixval));
			}
			System.out.println();
		}
	}

	public static char karakter(double ertek){
		char kar = ' ';
		if(ertek >= 240){
			kar = ' ';
		}else if(ertek >= 210){
			kar = '.';
		}else if(ertek >= 190){
			kar = '*';
		}else if(ertek >= 170){
			kar = '+';
		}else if(ertek >= 120){
			kar = '^';
		}else if(ertek >= 110){
			kar = '&';
		}else if(ertek >= 80){
			kar = '8';
		}else if(ertek >= 60){
			kar = '#';
		}else{
			kar = '@';
		}
		return kar;
	}

	public static void main(String[] args){
		try{
		File input = new File("grogu_a.jpg");
		BufferedImage image = ImageIO.read(input);
		convert(image);

		}catch(Exception e){
			System.out.println("Error. " + e);
		}
	}
}










































