/*#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int main (void)
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = 0;
    int y = 0;

    int deltax = 1;
    int deltay = 1;

    int windowx;
    int windowy;

    for (;;) {

        getmaxyx (ablak, windowy , windowx);
        mvprintw (y, x, "O");
        refresh ();
        usleep (100000);
        clear();

        x += deltax;
        y += deltay;

        if (x >= windowx-1) {
            deltax = deltax * -1;
        }
        if (x <= 0) {
            deltax = deltax * -1;
        }
        if (y <= 0) {
            deltay = deltay * -1;
        }
        if (y >= windowy-1) {
            deltay = deltay * -1;
        }

    }

    return 0;
}*/

#include <stdio.h>
#include <curses.h>
#include <unistd.h>
#include <stdlib.h>

// FORDÍTÁSNÁL KELL AZ -LNCURSES!!

int main (void)
{
    WINDOW *ablak; 
    ablak = initscr (); 

    int x = 0; //behozzuk a három pár változót
    int y = 0; //a jelenlegi pozíció

    int deltax = 1; //a lépésköz
    int deltay = 1;

    int windowx; 
    int windowy;

    for (;;) {

        getmaxyx (ablak, windowy , windowx); //eltároljuk a "játéktér" dimenzióit, ami itt az ablak nagysága
        mvprintw (y, x, "O"); //kiírja a labdát
        refresh (); //lefrissíti a játékteret, hogy ténylegesen kiíródjon a labda
        usleep (100000); //a léptetés gyakorisága
        clear(); //letörli a játékteret

        x += deltax; //a labda léptetése
        y += deltay;
	
	//ha eléri a határokat megfordítják a lépésirányt
        
	switch(x>=windowx-1 || x<=0) //oldalhatár
	case true:
            deltax = deltax * -1;

	switch(y>=windowy-1 || y<=0) //felső- és alsóhatár
	case true:
            deltay = deltay * -1;
    }

    return 0;
}
