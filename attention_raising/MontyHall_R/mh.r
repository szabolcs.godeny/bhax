#   An illustration written in R for the Monty Hall Problem 
#   Copyright (C) 2019  Dr. Norbert Bátfai, nbatfai@gmail.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>
#
#   https://bhaxor.blog.hu/2019/01/03/erdos_pal_mit_keresett_a_nagykonyvben_a_monty_hall-paradoxon_kapcsan
# 

kiserletek_szama=10000 # triviális
kiserlet = sample(1:3, kiserletek_szama, replace=T) # Mind a 10000 esethez
# hozzárendel egy választást az 1,2,3 számokból.
jatekos = sample(1:3, kiserletek_szama, replace=T) # Mind a 10000 esethez
# hozzárendel egy választást az 1,2,3 számokból.
musorvezeto=vector(length = kiserletek_szama) # A mérete ugyanúgy a
# kiserletek_szama-val lesz egyenlő, de nem random feltöltéssel:

# Itt azt szimuláljuk, hogy a műsorvezető tudja a kisérlet és a játékos
# értékét, és ezek tudatában fog kinyitni egy ajtót.
for (i in 1:kiserletek_szama) {

    if(kiserlet[i]==jatekos[i]){ # Itt a játékos eltalálta a jó ajtót.
    
        mibol=setdiff(c(1,2,3), kiserlet[i]) # Ezért elég az egyiket
        # kivenni, a kettő közül bármelyiket kinyithatja.
    
    }else{ # Itt a játékos nem találta el a jó ajtót.
    
        mibol=setdiff(c(1,2,3), c(kiserlet[i], jatekos[i])) # Ezért a három
        # esetből ki kell venni a jó ajtót és a játékos által választottat is.
    
    }

    musorvezeto[i] = mibol[sample(1:length(mibol),1)] # Itt "választ".

}

nemvaltoztatesnyer= which(kiserlet==jatekos) # Ha a játékos eltalálta és nem
# változtat, akkor nyer.
valtoztat=vector(length = kiserletek_szama) # Egy új vektor, a mérete ugyanúgy
# a kiserletek_szama-val lesz egyenlő

for (i in 1:kiserletek_szama) {

    holvalt = setdiff(c(1,2,3), c(musorvezeto[i], jatekos[i])) # Ha változtat,
    # akkor azt nyilván csak a még nem választottra teheti, ezért kell kivenni
    # a műsorvezető és a játékos által választottakat.
    valtoztat[i] = holvalt[sample(1:length(holvalt),1)]
    
}

valtoztatesnyer = which(kiserlet==valtoztat) # Ahhoz, hogy változtatás után
# nyerhessen, az új választásnak a nyereménnyel kell azonosnak lennie.

#kiiratások
sprintf("Kiserletek szama: %i", kiserletek_szama)
length(nemvaltoztatesnyer)
length(valtoztatesnyer)
length(nemvaltoztatesnyer)/length(valtoztatesnyer)
length(nemvaltoztatesnyer)+length(valtoztatesnyer)
